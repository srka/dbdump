function updateToggleIcons(){
	$('.database-list .database-list-item').each(function(){
		var $row = $(this);
		var $checkbox = $row.find('input.db-name');
		var status = $checkbox.prop('checked');
		if(status){
			$row.find('.fa').removeClass('fa-toggle-off');
			$row.find('.fa').addClass('fa-toggle-on');
		}else{
			$row.find('.fa').removeClass('fa-toggle-on');
			$row.find('.fa').addClass('fa-toggle-off');
		}
	});
}

function toggleDatabase(row){
	var $row = $(row);
	var $checkbox = $row.find('input.db-name');
	var status = $checkbox.prop('checked');
	var new_status = !status;
	$row.find('input.db-name').prop('checked', new_status);
	if(new_status){
		$row.find('.fa').removeClass('fa-toggle-off');
		$row.find('.fa').addClass('fa-toggle-on');
	}else{
		$row.find('.fa').removeClass('fa-toggle-on');
		$row.find('.fa').addClass('fa-toggle-off');
	}
}

function toggleSelectAll(){
	var $button = $('#select-all-btn');
	var status = ($button.html() == 'Select all');

	$('.database-list .database-list-item').each(function(){
		var $row = $(this);
		var $checkbox = $row.find('input.db-name');
		$checkbox.prop('checked', status);
	});
	updateToggleIcons();

	if(status){
		$button.html('Unselect all');
	}else{
		$button.html('Select all')
	}
}

$(function(){
	updateToggleIcons();
});