var DumpDB = function (options) {
	this.options = $.extend({
		script_url: '',
		dump_location: ''
	}, options);

	this.working = false;
	this.remaining = 0;
};

DumpDB.prototype = {

	dump: function (name, btn_elem) {
		var globalThis = this;

		//$('#result').append('<p id="db-status-' + name + uniqueId + '">Dumping database: "' + name + '"</p>');
		var $checkbox = $(".database-list-item input[name='" + name + "']");
		$checkbox.parents('.database-list-item').removeClass('done');
		$checkbox.parents('.database-list-item').addClass('loading');

		$.post(this.options.script_url, {dbname: name, location: this.options.dump_location}, function (data) {
			//$('#db-status-' + name + uniqueId).append(' <span>Done</span>');
			$checkbox.parents('.database-list-item').removeClass('loading');
			$checkbox.parents('.database-list-item').addClass('done');
			globalThis.remaining--;
			console.log(globalThis.remaining);
			if(globalThis.remaining == 0){
				globalThis.working = false;
				$(btn_elem).removeClass('disabled');
			}
		});
	},

	dumpSelected: function (selector, btn_elem) {
		var globalThis = this;
		if(!globalThis.working){
			var $rows = $('.' + selector);
			globalThis.working = true;
			globalThis.remaining = 0;
			$(btn_elem).addClass('disabled');
			$rows.each(function (index, element) {
				if (element.checked) {
					//alert('Dumping ' + $(element).attr('id'));
					globalThis.remaining++;
					globalThis.dump($(element).attr('name'), btn_elem);
				}
			});
		}
	},

	setLocation: function (path) {
		this.options.dump_location = path;
	}

};