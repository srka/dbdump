<?php
/**
 * User: Srdjan
 * Date: 2/14/13
 * Dump script called by AJAX
 */

error_reporting(0);

require_once('config.php');
require_once('database.php');

require_once('../config.php');
$database = new Database($config);

if (isset($_REQUEST['dbname']) && !empty($_REQUEST['dbname'])) {
	$database->dumpDatabase($_REQUEST['dbname'], $_REQUEST['location']);
	echo 'Database ' . stripslashes($_REQUEST['dbname']) . ' dumped successfully.';
} else {
	echo 'ERROR! Plese specify database name.';
}