<?php

/**
 * User: Srdjan
 * Date: 1/24/13
 * Time: 11:46 AM
 * Config file
 */
class Config
{
	private $_base_url;
	private $_db_hostname;
	private $_db_user;
	private $_db_password;
	private $_skin;
	private $_mysql_location;

	function __construct($base_url, $db_hostname, $db_user, $db_password, $mysql_location, $skin = 'default')
	{
		$this->_base_url = $base_url;
		$this->_db_hostname = $db_hostname;
		$this->_db_user = $db_user;
		$this->_db_password = $db_password;
		$this->_skin = $skin;
		if(!empty($mysql_location)){
			$this->_mysql_location = rtrim($mysql_location, '\\') . '\\';
		}else{
			$this->_mysql_location = '';
		}
	}

	function getBaseUrl()
	{
		return $this->_base_url;
	}

	function getDBHost()
	{
		return $this->_db_hostname;
	}

	function getDBUser()
	{
		return $this->_db_user;
	}

	function getDBPassword()
	{
		return $this->_db_password;
	}

	function  getSkin()
	{
		return $this->_skin;
	}

	function getMysqlLocation()
	{
		return $this->_mysql_location;
	}

}