<?php

/**
 * User: Srdjan
 * Date: 1/24/13
 * Time: 12:12 PM
 * Database class is the main class of the app
 */
class Database
{
	private $_connection;
	private $_config;
	private $_db_hostname;
	private $_db_user;
	private $_db_password;

	function __construct($config)
	{
		$this->_config = $config;
		$this->_db_hostname = $config->getDBHost();
		$this->_db_user = $config->getDBUser();
		$this->_db_password = $config->getDBPassword();

		$this->_connection = new mysqli($this->_db_hostname, $this->_db_user, $this->_db_password);
		return $this->_connection;
	}

	function getDatabaseList()
	{
		$result = $this->_connection->query('SHOW DATABASES');
		if ($result->num_rows > 0) {
			$databases = array();
			while ($row = $result->fetch_array()) {
				$databaseName = $row[0];
				if($databaseName != 'information_schema' && $databaseName != 'performance_schema' && $databaseName != 'mysql'){
					$databases[] = $databaseName;
				}
			}
			return $databases;
		} else {
			return false;
		}
	}

	function getDatabaseListHTML()
	{
		$databases = $this->getDatabaseList();
		if ($databases) {
			echo '<ul class="database-list">';
			$i = 0;
			foreach ($databases as $databaseName) {
				$i++;
				echo "<li class='database-list-item noselect' data-id='$i' onclick='toggleDatabase(this)'><i class='fa fa-toggle-off'></i> <input type='checkbox' name='$databaseName' class='db-name no-display' value='' autocomplete='off' /><label>$databaseName</label></li>";
			}
			echo '</ul>';
		}
	}

	function dumpAllDatabases()
	{
		$databases = $this->getDatabaseList();
		if ($databases) {
			foreach ($databases as $databaseName) {
				$exportPath = $databaseName . '.mysql';
				$this->_connection->query('mysqldump --opt -h' . $this->_config->getDBHost() . ' -u' . $this->_config->getDBUser() . ' -p' . $this->_config->getDBPassword() . ' ' . $databaseName . ' > ~/' . $exportPath);
			}
		}
	}

	function dumpDatabase($databaseName, $dumpLocation = '')
	{
		if(!empty($dumpLocation)){
			$dumpLocation = rtrim($dumpLocation, '\\') . '\\';
		}else{
			$dumpLocation = '../dump/';
		}
		$escapedDatabaseName = $this->_connection->real_escape_string($databaseName);
		$exportPath = '"' . $dumpLocation . $escapedDatabaseName . '.sql"';
		//$this->_connection->query('mysqldump --opt -h' . $this->_config->getDBHost() .' -u' . $this->_config->getDBUser() .' -p' . $this->_config->getDBPassword() .' ' . $databaseName .' > ~/' . $exportPath);
		if (empty($this->_db_password)) {
			passthru($this->_config->getMysqlLocation() . "mysqldump.exe --opt --host=" . $this->_config->getDBHost() . " --user=" . $this->_config->getDBUser() . " " . $escapedDatabaseName . " > " . $exportPath);
		} else {
			passthru($this->_config->getMysqlLocation() . "mysqldump.exe --opt --host=" . $this->_config->getDBHost() . " --user=" . $this->_config->getDBUser() . " --password=" . $this->_config->getDBPassword() . " " . $escapedDatabaseName . " > " . $exportPath);
		}
	}
}