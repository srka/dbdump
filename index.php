<?php
/**
 * User: Srdjan
 * Date: 1/24/13
 * Time: 12:03 PM
 * Index
 */

error_reporting(0);

require_once('app/config.php');
require_once('app/database.php');

require_once('config.php');
$database = new Database($config);
//$database->dumpDatabase('mage');
?>
<!doctype html>
<html>
<head>
	<title>DB Dump</title>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">

	<link rel="stylesheet" type="text/css" href="skins/<?php echo strtolower($config->getSkin()) ?>/css/style.css" />

	<script type="text/javascript" src="<?php $config->getBaseUrl() ?>js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php $config->getBaseUrl() ?>js/dump.js"></script>
	<script type="text/javascript" src="<?php $config->getBaseUrl() ?>js/app.js"></script>
	<script type="text/javascript">
		var dump;
		$(document).ready(function (e) {
			dump = new DumpDB({script_url: "<?php echo $config->getBaseUrl() ?>/app/dump.php"});
			//dump.dump('mage');
		});
	</script>
</head>
<body>

	<div id="header" class="clearfix">
		<h1 id="logo"><i class="fa fa-database"></i> DB Dump</h1>
		<div id="input-wrapper">
			<input type="text" name="location-input" id="location-input" class="input-text" value="" onChange="dump.setLocation(this.value)" placeholder="Dump location..." />
		</div>
		<button type="button" name="dump" id="dump-button" value="" onClick="dump.dumpSelected('db-name', this)"><i class="fa fa-download"></i> Dump</button>
	</div>

	<div id="main">
		<h2>Databases</h2>
		<a id="select-all-btn" href="javascript:void(0)" onclick="toggleSelectAll()">Select all</a>
		<?php $database->getDatabaseListHTML(); ?>
		<div id="result"></div>
	</div>

</body>
</html>
